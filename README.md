# Info
Based on Ubuntu 18 - Bionic

PHP 7.2 + Apache 2 + XDebug

PHP Modules: mysqli, xml, xmlrpc, curl, gd, imagic, imap, mbstring, opcache, soap, zip, intl, zmq, sqlite

Tools: 
mc, vim, nano, lynx

Esposed ports: 80, 443, 9000 (for XDebug)

Apache includes all configuration (*.conf) files from directory /etc/apache2/vhost. One can map it to the local directory with vhosts definitions.

PHP php.ini file includes configuration options for XDebug. Remote debugging is not yet tested .

Example run command: 
```sh
docker run -d --name webdev7.2 -p 80:80 -p 9000:9000 -v ~/vhosts:/etc/apache2/vhosts -v ~/project_dir/www/html:/var/www/html webdev7.2:latest
```

# Still to do
- Test remote debugging
- Test docker swarm integration with mysql and phpymyadmin

